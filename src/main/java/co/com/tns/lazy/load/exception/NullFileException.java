package co.com.tns.lazy.load.exception;

public class NullFileException extends BusinessException {

    public NullFileException(String message) {
        super(message);
    }
}

package co.com.tns.lazy.load.exception;

public class EmptyFileException extends BusinessException {
	public EmptyFileException(String message) {
		super(message);
	}
}

package co.com.tns.lazy.load.exception;

public class InvalidTypeFileException extends BusinessException {
	public InvalidTypeFileException(String message) {
		super(message);
	}
}

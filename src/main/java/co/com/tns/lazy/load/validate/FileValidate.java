package co.com.tns.lazy.load.validate;

import co.com.tns.lazy.load.exception.BusinessException;
import co.com.tns.lazy.load.util.Constants;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

@Service
public class FileValidate {

	public boolean isNullFile(File file) {
		return null == file;
	}

	public boolean isEmptyFile(File file) {
		return (file.length() == 0);
	}

	public boolean onlyHasNumbers(File file) throws BusinessException {
		boolean onlyHasNumbers = true;
		try (Scanner lector = new Scanner(file)) {
			while (lector.hasNextLine()) {
				String line = lector.nextLine();
				if (!line.matches("[0-9]+")) {
					onlyHasNumbers = false;
				}
			}
		} catch (IOException exeption) {
			throw new BusinessException(Constants.FILE_ERROR_NO_NUMBER);
		}
		return onlyHasNumbers;
	}

	public boolean isValidateTypeFile(String file) {
		String tipeFileTXT = file.substring(file.lastIndexOf(Constants.SYMBOL_DOT));
		return (tipeFileTXT.equalsIgnoreCase(Constants.VALID_EXTENSION_FILE) );
	}
}
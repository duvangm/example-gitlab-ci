package co.com.tns.lazy.load.util;

import co.com.tns.lazy.load.exception.BusinessException;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public final class FileUtil {

	private FileUtil() {
		super();
	}

	public static File convertToFile(MultipartFile file) throws BusinessException {
		File convFile = new File(file.getOriginalFilename());

		try(FileOutputStream fos = new FileOutputStream(convFile)){
			if(convFile.createNewFile()){
				throw new BusinessException(Constants.ERROR_CREATING_FILE);
			}
			fos.write(file.getBytes());
			return convFile;

		} catch (IOException e) {
			throw new BusinessException(Constants.FILE_ERROR, e);
		}
	}
}

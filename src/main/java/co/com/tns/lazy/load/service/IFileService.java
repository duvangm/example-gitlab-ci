package co.com.tns.lazy.load.service;

import co.com.tns.lazy.load.exception.BusinessException;

import java.io.File;

public interface IFileService {

	String maximizeElementsByDay(File file) throws BusinessException;

}

package co.com.tns.lazy.load.service;

import co.com.tns.lazy.load.business.LazyLoad;
import co.com.tns.lazy.load.exception.BusinessException;
import co.com.tns.lazy.load.manager.FileManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FileServiceTest {

    @InjectMocks
    FileService fileService;

    @Mock
    FileManager fileManager;

    @Mock
    LazyLoad lazyLoad;

    private File fileValid = new File(getClass().getClassLoader().getResource("lazy_loading_example_input.txt").getFile());


    @Test
    public void shouldMaximizeTripsByDay() throws BusinessException {
        List<Integer> fileInList = new ArrayList<>();

        fileInList.add(1);
        fileInList.add(1);
        fileInList.add(50);

        when(fileManager.convertFileToList(Mockito.any())).thenReturn(fileInList);
        when(lazyLoad.maximizeElementsByDay(fileInList)).thenReturn("Case #1: 1");
        fileService.maximizeElementsByDay(fileValid);
        Mockito.verify(fileManager,times(1)).convertFileToList(Mockito.any());
        Mockito.verify(fileManager,times(1)).validatefile(Mockito.any());
        Mockito.verify(lazyLoad,times(1)).maximizeElementsByDay(fileInList);
    }

}

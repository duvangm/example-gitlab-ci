package co.com.tns.lazy.load.manager;

import co.com.tns.lazy.load.exception.BusinessException;
import co.com.tns.lazy.load.exception.EmptyFileException;
import co.com.tns.lazy.load.exception.InvalidTypeFileException;
import co.com.tns.lazy.load.exception.NullFileException;
import co.com.tns.lazy.load.validate.FileValidate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FileManagerTest {

	@InjectMocks
	private FileManager fileManager;

	@Mock
	private FileValidate fileValidate;

	private File emptyFile = new File(getClass().getClassLoader().getResource("emptyFile.txt").getFile());
	private File invalidTypeFile = new File(getClass().getClassLoader().getResource("invalidFileType.jpg").getFile());
	private File fileValid = new File(getClass().getClassLoader().getResource("lazy_loading_example_input.txt").getFile());

	@Test
	public void shouldConvertFileToList() throws BusinessException {
		List<Integer> archivoEnTipoLista = Arrays.asList(5, 4, 30, 30, 1, 1, 3, 20, 20, 20, 11, 1, 2, 3, 4, 5, 6, 7, 8,
				9, 10, 11, 6, 9, 19, 29, 39, 49, 59, 10, 32, 56, 76, 8, 44, 60, 47, 85, 71, 91);
		List<Integer> archivoEnTipoListaResult = fileManager.convertFileToList(fileValid);
		assertEquals(archivoEnTipoLista, archivoEnTipoListaResult);
	}

	@Test(expected = NullFileException.class)
	public void shouldThrowNullBusinessException() throws BusinessException {
		when(fileValidate.isNullFile(any())).thenReturn(true);
		fileManager.validatefile(null);
	}

	@Test(expected = InvalidTypeFileException.class)
	public void shouldThrowsInvalidTypeFileException() throws BusinessException {
		fileManager.validatefile(invalidTypeFile);
	}

	@Test(expected = EmptyFileException.class)
	public void shouldThrowsEmptyFileException() throws BusinessException {
		when(fileValidate.isEmptyFile(any())).thenReturn(true);
		fileManager.validatefile(emptyFile);
	}

}
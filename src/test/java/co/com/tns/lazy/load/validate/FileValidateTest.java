package co.com.tns.lazy.load.validate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import co.com.tns.lazy.load.exception.BusinessException;

import java.io.File;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class FileValidateTest {

	private File file = new File(getClass().getClassLoader().getResource("lazy_loading_example_input.txt").getFile());
	private File invalidTypeFile = new File(getClass().getClassLoader().getResource("invalidFileType.jpg").getFile());

	@InjectMocks
	private FileValidate fileValidate;

	@Test
	public void shouldValidateIsNotNullFile() {
		boolean resutlNotNullFile = fileValidate.isNullFile(file);
		assertFalse(resutlNotNullFile);
	}

	@Test
	public void shouldValidateFileNull() {
		boolean resultFileNull = fileValidate.isNullFile(null);
		assertTrue(resultFileNull);
	}

	@Test
	public void shouldValidateRightFileExtension() {
		boolean isRight = fileValidate.isValidateTypeFile(file.getName());
		assertTrue(isRight);
	}

	@Test
	public void shouldValidateWrongFileExtension() {
		boolean isWrong = fileValidate.isValidateTypeFile(invalidTypeFile.getName());
		assertFalse(isWrong);
	}

	@Test
	public void shouldValidateEmptyFile() {
		File emptyFile = new File(getClass().getClassLoader().getResource("emptyFile.txt").getFile());
		boolean isEmpty = fileValidate.isEmptyFile(emptyFile);
		assertTrue(isEmpty);
	}

	@Test
	public void shouldValidateOnlyHaveNumbers() throws BusinessException {
		File fileWithNumbers = new File(
				getClass().getClassLoader().getResource("lazy_loading_example_input.txt").getFile());
		boolean onlynumbers = fileValidate.onlyHasNumbers(fileWithNumbers);
		assertTrue(onlynumbers);
	}

	@Test
	public void shouldValidateNotEmptyFile() {
		boolean isNotEmpty = fileValidate.isEmptyFile(file);
		assertFalse(isNotEmpty);
	}
}
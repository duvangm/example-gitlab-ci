package co.com.tns.lazy.load.business;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TripTest {

	private Trip trip = new Trip();

	@Test
	public void shouldSortAList() {
		List<Integer> listOfWeights = Arrays.asList(5, 3, 12, 14, 1);
		List<Integer> listOfWeightsOrdered = Arrays.asList(14, 12, 5, 3, 1);
		trip.sortListOfWeights(listOfWeights);
		assertEquals(listOfWeights, listOfWeightsOrdered);
	}

	@Test
	public void shouldCountTripsWithOnlyWeight() {
		List<Integer> weights = Arrays.asList(50);
		int trips = trip.getNumberOfTrips(weights);
		assertEquals(1, trips);
	}
}
